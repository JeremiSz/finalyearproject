# Vector Graphics GPU Rendering Method Comparison

![Poster](https://gitlab.com/JeremiSz/finalyearproject/-/raw/main/figures/Jeremi_Portrait_Final_edited.png?inline=false)
Poster edited by [Jeremi Szlapka](https://www.linkedin.com/in/jeremi-szlapka/) based on design by [Rosie Dempsey](https://www.linkedin.com/in/rosie-dempsey-30344622b/). 

For more details, please see the [report](https://gitlab.com/JeremiSz/finalyearproject/-/blob/main/Jeremi_Szlapka_t00210844_FYP_Spline_Rendering.pdf).

Code forked from [tutorial series](https://www.youtube.com/playlist?list=PL8327DO66nu9qYVKLDmdLW_84-yE4auCR) by Brendan Galea.