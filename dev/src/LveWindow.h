//
// Created by root on 20/11/22.
//

#ifndef DEV_LVEWINDOW_H
#define DEV_LVEWINDOW_H
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <string>

namespace lve{
    class LveWindow{
    public:
        LveWindow(int width,int height,std::string name);
        ~LveWindow();
        LveWindow(const LveWindow &) = delete;
        LveWindow &operator = (const LveWindow &) = delete;
        bool shouldClose();
        void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);
        VkExtent2D getExtent();
        bool wasWindowResized(){return frameBufferResized;}
        void resetWindowResizedFlag(){frameBufferResized = false;}
        GLFWwindow *getGLFWwindow() const {return window;};
    private:
        static void frameBufferResizeCallback(GLFWwindow *window,int width,int height);
        void initWindow();
        int width;
        int height;
        bool frameBufferResized = false;
        std::string windowName;
        GLFWwindow *window;
    };
}

#endif //DEV_LVEWINDOW_H
