//
// Created by vox on 13/01/23.
//

#ifndef DEV_LVEFRAMEINFO_H
#define DEV_LVEFRAMEINFO_H

#include "vulkan/vulkan.h"
#include "renderables/LveVectorGraphic.h"

namespace lve{
    struct FrameInfo{
        int frameIndex;
        float frameTime;
        VkCommandBuffer commandBuffer;
        std::vector<LveVectorGraphic> &graphics;
    };
}

#endif //DEV_LVEFRAMEINFO_H
