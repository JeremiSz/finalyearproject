//
// Created by root on 20/11/22.
//

#include "LvePipeline.h"
#include "renderables/LvePath.h"

#include <fstream>
#include <stdexcept>
#include <iostream>
#include <cassert>

namespace lve {

    LvePipeline::LvePipeline(LveDevice &device,
                             const std::string& vertFilepath,
                             const std::string& fragFilepath,
                             const std::string* geomFilepath,
                             const std::string* tescFilepath,
                             const std::string* teseFilepath,
                             const PipelineConfigInfo& configInfo)
                             :lveDevice{device} {
        createGraphicsPipeline(vertFilepath,fragFilepath,geomFilepath,tescFilepath,teseFilepath,configInfo);
    }
    LvePipeline::~LvePipeline() {
        vkDestroyShaderModule(lveDevice.device(),vertShaderModule, nullptr);
        vkDestroyShaderModule(lveDevice.device(),fragShaderModule, nullptr);
        if (hasGeom) {
            vkDestroyShaderModule(lveDevice.device(), geomShaderModule, nullptr);
        }
        if (hasTess) {
            vkDestroyShaderModule(lveDevice.device(), tescShaderModule, nullptr);
            vkDestroyShaderModule(lveDevice.device(), teseShaderModule, nullptr);
        }
        vkDestroyPipeline(lveDevice.device(),graphicsPipeline, nullptr);

    }
    std::vector<char> LvePipeline::readFile(const std::string& filepath){
         std::ifstream file{filepath,std::ios::ate | std::ios::binary};
         if (!file.is_open()){
             throw std::runtime_error("failed to open file " + filepath);
         }
         size_t fileSize = static_cast<size_t>(file.tellg());
         std::vector<char> buffer(fileSize);

         file.seekg(0);
         file.read(buffer.data(),fileSize);

         file.close();
         return buffer;
     }

    void LvePipeline::createGraphicsPipeline(const std::string& vertFilepath,
                                             const std::string& fragFilepath,
                                             const std::string* geomFilepath,
                                             const std::string* tescFilepath,
                                             const std::string* teseFilepath,
                                             const PipelineConfigInfo& configInfo){
        assert(configInfo.pipelineLayout != VK_NULL_HANDLE && "Cannot create graphics pipeline: no pipeline layout");
        assert(configInfo.renderPass != VK_NULL_HANDLE && "Cannot create graphics pipeline: no renderpass");
        unsigned char shaderStage = 2 + (geomFilepath == nullptr?0:1) + (teseFilepath == nullptr || tescFilepath == nullptr?0:2);

        VkPipelineShaderStageCreateInfo shaderStages[shaderStage];
        VkGraphicsPipelineCreateInfo pipelineInfo{};

        shaderStage = 0;
        const auto functionName = "main";

        auto vertCode = readFile(vertFilepath);
        createShaderModule(vertCode,&vertShaderModule);
        shaderStages[shaderStage] =
                {
                VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                nullptr,0,
                VK_SHADER_STAGE_VERTEX_BIT,vertShaderModule,
                functionName, nullptr
                };
        shaderStage++;

        auto fragCode = readFile(fragFilepath);
        createShaderModule(fragCode,&fragShaderModule);
        shaderStages[shaderStage] = {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,0,
        VK_SHADER_STAGE_FRAGMENT_BIT,fragShaderModule,
        functionName, nullptr
        };
        shaderStage++;

        if (geomFilepath != nullptr) {
            auto gemoCode = readFile(*geomFilepath);
            createShaderModule(gemoCode, &geomShaderModule);
            shaderStages[shaderStage] ={
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,0,
        VK_SHADER_STAGE_GEOMETRY_BIT,geomShaderModule,
        functionName, nullptr
            };
            shaderStage++;
            hasGeom = true;
        }
        if(teseFilepath != nullptr && tescFilepath != nullptr){
            auto teseCode = readFile(*teseFilepath);
            createShaderModule(teseCode,&teseShaderModule);
            shaderStages[shaderStage]={
                    VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                    nullptr,0,
                    VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT,teseShaderModule,
                    functionName, nullptr
            };
            shaderStage++;

            auto tescCode= readFile(*tescFilepath);
            createShaderModule(tescCode,&tescShaderModule);
            shaderStages[shaderStage]={
                    VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                    nullptr,0,
                    VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT,tescShaderModule,
                    functionName, nullptr
            };
            shaderStage++;

            VkPipelineTessellationStateCreateInfo tessellationStateCreateInfo{};
            tessellationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
            tessellationStateCreateInfo.patchControlPoints = 4;
            pipelineInfo.pTessellationState = &tessellationStateCreateInfo;
            hasTess = true;
        }



        auto& bindingDescriptions = configInfo.bindingDescriptions;
        auto& attributeDescriptions = configInfo.attributeDescriptions;
        VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.vertexBindingDescriptionCount = static_cast<uint32_t>(bindingDescriptions.size());
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
        vertexInputInfo.pVertexBindingDescriptions = bindingDescriptions.data();

        pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineInfo.stageCount = shaderStage;
        pipelineInfo.pStages = shaderStages;
        pipelineInfo.pVertexInputState = &vertexInputInfo;
        pipelineInfo.pInputAssemblyState = &configInfo.inputAssemblyInfo;
        pipelineInfo.pViewportState = &configInfo.viewportInfo;
        pipelineInfo.pRasterizationState = &configInfo.rasterizationInfo;
        pipelineInfo.pMultisampleState = &configInfo.multisampleInfo;
        pipelineInfo.pColorBlendState = &configInfo.colourBlendInfo;
        pipelineInfo.pDepthStencilState = &configInfo.depthStencilInfo;
        pipelineInfo.pDynamicState = &configInfo.dynamicStateInfo;
        pipelineInfo.layout = configInfo.pipelineLayout;
        pipelineInfo.renderPass = configInfo.renderPass;
        pipelineInfo.subpass = configInfo.subpass;

        pipelineInfo.basePipelineIndex = -1;
        pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

        if(vkCreateGraphicsPipelines(lveDevice.device(),VK_NULL_HANDLE,1,&pipelineInfo, nullptr,&graphicsPipeline) != VK_SUCCESS){
            throw std::runtime_error("failed to create graphics pipeline");
        }
    }

    void LvePipeline::createShaderModule(const std::vector<char>& code,VkShaderModule* shaderModule){
        VkShaderModuleCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

        if(vkCreateShaderModule(lveDevice.device(),&createInfo, nullptr,shaderModule) != VK_SUCCESS){
            throw std::runtime_error("Failed to create shader module");
        }
    }

    void LvePipeline::bind(VkCommandBuffer commandBuffer){
        vkCmdBindPipeline(commandBuffer,VK_PIPELINE_BIND_POINT_GRAPHICS,graphicsPipeline);
    }
    void LvePipeline::addTessellationConfigInfo(PipelineConfigInfo& configInfo){
        configInfo.inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        configInfo.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
        configInfo.inputAssemblyInfo.flags = 0;
        configInfo.inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;
    }
    void LvePipeline::addQuadSupport(PipelineConfigInfo& configInfo){
        configInfo.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
    }

    void LvePipeline::defaultPipelineConfigInfo(PipelineConfigInfo& configInfo){
        configInfo.inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        configInfo.inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        configInfo.inputAssemblyInfo.primitiveRestartEnable = VK_FALSE;

        configInfo.viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        configInfo.viewportInfo.viewportCount = 1;
        configInfo.viewportInfo.pViewports = nullptr;
        configInfo.viewportInfo.scissorCount = 1;
        configInfo.viewportInfo.pScissors = nullptr;

        configInfo.rasterizationInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        configInfo.rasterizationInfo.depthClampEnable = VK_FALSE;
        configInfo.rasterizationInfo.rasterizerDiscardEnable = VK_FALSE;
        configInfo.rasterizationInfo.polygonMode = VK_POLYGON_MODE_FILL;
        configInfo.rasterizationInfo.lineWidth = 1.0f;
        configInfo.rasterizationInfo.cullMode = VK_CULL_MODE_NONE;
        configInfo.rasterizationInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
        configInfo.rasterizationInfo.depthBiasEnable = VK_FALSE;
        configInfo.rasterizationInfo.depthBiasConstantFactor = 0.0f;
        configInfo.rasterizationInfo.depthBiasClamp = 0.0f;
        configInfo.rasterizationInfo.depthBiasSlopeFactor = 0.0f;

        configInfo.multisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        configInfo.multisampleInfo.sampleShadingEnable = VK_FALSE;
        configInfo.multisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        configInfo.multisampleInfo.minSampleShading = 1.0f;
        configInfo.multisampleInfo.pSampleMask = nullptr;
        configInfo.multisampleInfo.alphaToCoverageEnable = VK_FALSE;
        configInfo.multisampleInfo.alphaToOneEnable = VK_FALSE;

        configInfo.colourBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        configInfo.colourBlendAttachment.blendEnable = VK_FALSE;
        configInfo.colourBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        configInfo.colourBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        configInfo.colourBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
        configInfo.colourBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        configInfo.colourBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        configInfo.colourBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

        configInfo.colourBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        configInfo.colourBlendInfo.logicOpEnable = VK_FALSE;
        configInfo.colourBlendInfo.logicOp = VK_LOGIC_OP_COPY;
        configInfo.colourBlendInfo.attachmentCount = 1;
        configInfo.colourBlendInfo.pAttachments = &configInfo.colourBlendAttachment;
        configInfo.colourBlendInfo.blendConstants[0] = 0.0f;
        configInfo.colourBlendInfo.blendConstants[1] = 0.0f;
        configInfo.colourBlendInfo.blendConstants[2] = 0.0f;
        configInfo.colourBlendInfo.blendConstants[3] = 0.0f;

        configInfo.depthStencilInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        configInfo.depthStencilInfo.depthTestEnable = VK_TRUE;
        configInfo.depthStencilInfo.depthWriteEnable = VK_TRUE;
        configInfo.depthStencilInfo.depthCompareOp = VK_COMPARE_OP_LESS;
        configInfo.depthStencilInfo.depthBoundsTestEnable = VK_FALSE;
        configInfo.depthStencilInfo.minDepthBounds = 0.0f;
        configInfo.depthStencilInfo.maxDepthBounds = 1.0f;
        configInfo.depthStencilInfo.stencilTestEnable = VK_FALSE;
        configInfo.depthStencilInfo.front = {};
        configInfo.depthStencilInfo.back = {};

        configInfo.dynamicStateEnables = {VK_DYNAMIC_STATE_VIEWPORT,VK_DYNAMIC_STATE_SCISSOR};
        configInfo.dynamicStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        configInfo.dynamicStateInfo.pDynamicStates = configInfo.dynamicStateEnables.data();
        configInfo.dynamicStateInfo.dynamicStateCount = static_cast<uint32_t>(configInfo.dynamicStateEnables.size());
        configInfo.dynamicStateInfo.flags = 0;

        configInfo.bindingDescriptions =LvePath::Vertex::getBindingDescriptions();
        configInfo.attributeDescriptions = LvePath::Vertex::getAttributeDescriptions();
    }
} // lve