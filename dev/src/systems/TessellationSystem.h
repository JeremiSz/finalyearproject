//
// Created by vox on 06/02/23.
//

#ifndef DEV_TesselationSystem_H
#define DEV_TesselationSystem_H

#include <memory>
#include "../LveCamera.h"
#include "../LveDevice.h"
#include "../LvePipeline.h"
#include "../LveFrameInfo.h"

namespace lve {

    class TessellationSystem {
    public:
        TessellationSystem(LveDevice& device, VkRenderPass renderPass);
        ~TessellationSystem();
        TessellationSystem(const TessellationSystem &) = delete;
        TessellationSystem &operator = (const TessellationSystem &) = delete;

        void render(FrameInfo &frameInfo);
    private:
        std::string fragFilepath = "shaders/TessellationShader.frag.spv";
        std::string vertFilepath = "shaders/TessellationShader.vert.spv";
        std::string teseFilepath = "shaders/TessellationShader.tese.spv";
        std::string tescFilepath = "shaders/TessellationShader.tesc.spv";
        void createPipelineLayout();
        void createPipeline(VkRenderPass renderPass);
        LveDevice& lveDevice;
        std::unique_ptr<LvePipeline> lvePipeline;
        VkPipelineLayout pipelineLayout;
    };


} // lve

#endif //DEV_TesselationSystem_H
