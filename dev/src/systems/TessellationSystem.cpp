//
// Created by vox on 06/02/23.
//

#include "TessellationSystem.h"
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

namespace lve {
    struct SimplePushConstantDataVector{
        glm::mat4 modelMatrix{1.f};
    };
    TessellationSystem::TessellationSystem(
            LveDevice &device,
            VkRenderPass renderPass
            ): lveDevice{device}
    {
        createPipelineLayout();
        createPipeline(renderPass);
    }
    TessellationSystem::~TessellationSystem() {
        vkDestroyPipelineLayout(lveDevice.device(),pipelineLayout, nullptr);
    }
    void TessellationSystem::createPipelineLayout() {
        VkPushConstantRange pushConstantRange{};
        pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        pushConstantRange.offset = 0;
        pushConstantRange.size = sizeof(SimplePushConstantDataVector);

        VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
        pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        pipelineLayoutInfo.setLayoutCount = 0;
        pipelineLayoutInfo.pSetLayouts = nullptr;
        pipelineLayoutInfo.pushConstantRangeCount = 1;
        pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
        if(vkCreatePipelineLayout(lveDevice.device(),&pipelineLayoutInfo, nullptr,&pipelineLayout) != VK_SUCCESS){
            throw std::runtime_error("failed to create layout");
        }
    }
    void TessellationSystem::createPipeline(VkRenderPass renderPass) {
        assert(pipelineLayout != nullptr && "Cannot create pipeline before pipeline layout");

        PipelineConfigInfo pipelineConfig{};
        LvePipeline::defaultPipelineConfigInfo(pipelineConfig);

        LvePipeline::addTessellationConfigInfo(pipelineConfig);

        pipelineConfig.renderPass = renderPass;
        pipelineConfig.pipelineLayout = pipelineLayout;
        lvePipeline = std::make_unique<LvePipeline>(
                lveDevice,
                vertFilepath,
                fragFilepath,
                nullptr,
                &tescFilepath,
                &teseFilepath,
                pipelineConfig);
    }
    void TessellationSystem::render(FrameInfo &frameInfo) {
        lvePipeline->bind(frameInfo.commandBuffer);
        for(auto& obj: frameInfo.graphics){
            SimplePushConstantDataVector push{};
            push.modelMatrix = obj.transform.mat4();
            vkCmdPushConstants(
                    frameInfo.commandBuffer,
                    pipelineLayout,
                    VK_SHADER_STAGE_VERTEX_BIT,
                    0,
                    sizeof(SimplePushConstantDataVector),
                    &push
            );
            obj.model->bind(frameInfo.commandBuffer);
            obj.model->draw(frameInfo.commandBuffer);
        }
    }

} // lve