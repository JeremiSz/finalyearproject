//
// Created by vox on 12/02/23.
//

#include "TessellationApp.h"
#include "../LveFrameInfo.h"
#include "TessellationSystem.h"

#include <array>
#include <chrono>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

namespace lve {
    TessellationApp::TessellationApp(int device) : lveDevice(lveWindow, false, true,device){
        loadObjects();
    }

    TessellationApp::~TessellationApp() {}

    void TessellationApp::run() {
        TessellationSystem tesselationSystem{lveDevice, lveRenderer.getSwapChainRenderPass()};
        auto startTime = std::chrono::high_resolution_clock::now();
        while (!lveWindow.shouldClose()){
            glfwPollEvents();
            auto newTime = std::chrono::high_resolution_clock::now();
            float frameTime = std::chrono::duration<float,std::chrono::seconds::period>(newTime - startTime).count();
            startTime = newTime;

            if (auto commandBuffer = lveRenderer.beginFrame()){
                int frameIndex = lveRenderer.getFrameIndex();
                FrameInfo frameInfo{
                        frameIndex,
                        frameTime,
                        commandBuffer,
                        graphics
                };
                //render
                lveRenderer.beginSwapChainRenderPass(commandBuffer);
                tesselationSystem.render(frameInfo);
                lveRenderer.endSwapChainRenderPass(commandBuffer);
                lveRenderer.endFrame();
            }
        }
        vkDeviceWaitIdle(lveDevice.device());
    }
    void TessellationApp::profile(unsigned int iterations){
        TessellationSystem tessellationSystem{lveDevice, lveRenderer.getSwapChainRenderPass()};
        auto startTime = std::chrono::high_resolution_clock::now();
        for (unsigned int frame = 0;frame<iterations;frame++){
            if (auto commandBuffer = lveRenderer.beginFrame()){
                int frameIndex = lveRenderer.getFrameIndex();
                FrameInfo frameInfo{
                        frameIndex,
                        0,
                        commandBuffer,
                        graphics
                };
                //render
                lveRenderer.beginSwapChainRenderPass(commandBuffer);
                tessellationSystem.render(frameInfo);
                lveRenderer.endSwapChainRenderPass(commandBuffer);
                lveRenderer.endFrame();
            }
        }

        auto newTime = std::chrono::high_resolution_clock::now();
        vkDeviceWaitIdle(lveDevice.device());
        float frameTime = std::chrono::duration<float,std::chrono::seconds::period>(newTime - startTime).count();
        printf("%i frames took %f seconds",iterations,frameTime);

    }
    std::string path = "m 84.824664,75.16629 c 0.195956,-1.409624 1.202316,-2.569106 2.438837,-2.639287 1.236521,-0.07018 3.136101,1.803948 2.953803,2.836381 -0.160294,0.907816 -1.025852,2.267201 -1.094995,3.059527 -0.202745,2.323293 0.473303,3.92985 -0.09626,3.920903 -0.663974,-0.01043 -0.366329,-1.145502 -0.436227,-3.843386 -0.01528,-0.589775 -0.503575,-0.818146 -0.703202,-0.64367 -0.736483,0.643694 -0.872506,3.386087 -0.856976,4.124856 0.03448,1.6403 2.251833,0.969794 2.372019,2.271792 0.03065,0.332057 0.08713,2.083786 0.143697,4.525933 0.03589,1.549448 1.391014,9.553532 0.43528,9.488651 -0.751214,-0.051 -0.377077,-4.41683 -0.375228,-4.1425 0.01627,2.413603 0.06686,5.028138 0.04864,7.4135 -0.04152,5.43762 1.278749,7.28555 0.337487,7.41837 -0.684126,0.0965 0.259049,-2.36923 -0.60931,-2.31172 -0.349297,0.0231 0.108374,2.89315 0.01943,3.03178 -0.04073,0.0635 -0.169438,-0.0194 0.14477,0.17102 0.184673,0.1119 0.525519,1.29176 -0.230467,1.02816 -1.709035,-0.5959 -5.578598,1.06777 -5.624228,0.0269 -0.03017,-0.68821 0.197924,-0.22091 0.344923,-0.97351 0.159542,-0.8168 -0.09374,-0.66403 0.626465,-4.30399 0.153039,-0.77347 0.420117,-0.62598 0.343651,-1.41752 -0.07593,-0.78604 0.240756,-1.39245 0.243231,-2.26783 0.0034,-1.20333 -0.282207,-2.704736 -0.228261,-3.969702 0.0435,-1.020013 0.08506,-2.039891 0.09505,-3.027103 0.0048,-0.470174 0.125688,0.977662 -0.06076,0.951386 -1.16836,-0.164657 -0.148041,-4.882159 0.113712,-6.400221 0.472965,-2.743011 0.160967,-4.855428 0.189463,-5.308402 0.04785,-0.760672 1.030452,-0.02974 0.968851,-2.271791 -0.03126,-1.137661 0.312902,-4.021027 -0.76234,-4.837921 -0.582755,-0.442737 -0.801308,-1.47718 -0.741052,-1.910635 z";
    void TessellationApp::loadObjects() {
        auto vase = loadObject(
                lveDevice,
                path,
                {0.f, 0.f, 0.f},
                {1.f, 1.f, 1.f}
        );
        graphics.push_back(std::move(vase));
    }
    LveVectorGraphic TessellationApp::loadObject(LveDevice& device, std::string& filepath,glm::vec3 translation,glm::vec3 scale){
        auto object = LveVectorGraphic();
        object.model = LvePath::createDefault(lveDevice);//LvePath::createModelFromFile(device,filepath);
        object.transform.translation = translation;
        object.transform.scale = scale;
        return object;
    }
} // lve