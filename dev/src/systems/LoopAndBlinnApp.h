//
// Created by vox on 12/02/23.
//

#ifndef DEV_LOOPANDBLINNAPP_H
#define DEV_LOOPANDBLINNAPP_H

#include "../LveWindow.h"
#include "../LveDevice.h"
#include "../LveRenderer.h"
#include "../LveDescriptors.h"
#include "../renderables/LveVectorGraphic.h"

#include <memory>
#include <vector>

namespace lve {

    class LoopAndBlinnApp {
    public:
        static constexpr int WIDTH = 800, HEIGHT = 800;
        void run();
        void profile(unsigned int i);
        explicit LoopAndBlinnApp(int deviceIndex);
        ~LoopAndBlinnApp();
        LoopAndBlinnApp(const LoopAndBlinnApp &) = delete;
        LoopAndBlinnApp &operator = (const LoopAndBlinnApp &) = delete;
    private:
        void loadObjects();
        LveVectorGraphic loadObject(LveDevice &device, std::string& filepath,glm::vec3 translation,glm::vec3 scale);

        LveWindow lveWindow{WIDTH,HEIGHT,"Hello Vulkan"};
        LveDevice lveDevice;
        LveRenderer lveRenderer{lveWindow,lveDevice};
        std::vector<LveVectorGraphic> graphics;

        LveVectorGraphic loadObject(LveDevice &device, std::vector<float> data, glm::vec3 translation, glm::vec3 scale);
    };

} // lve

#endif //DEV_LOOPANDBLINNAPP_H
