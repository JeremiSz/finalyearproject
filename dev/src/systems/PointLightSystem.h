//
// Created by vox on 12/01/23.
//

#ifndef DEV_POINTLIGHTSYSTEM_H
#define DEV_POINTLIGHTSYSTEM_H

#include "../LveCamera.h"
#include "../LveDevice.h"
#include "../LvePipeline.h"
#include "../renderables/LveObject.h"
#include "../LveFrameInfo.h"

namespace lve {

    class PointLightSystem {
    public:
        PointLightSystem(LveDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout);
        ~PointLightSystem();
        PointLightSystem(const PointLightSystem &) = delete;
        PointLightSystem &operator = (const PointLightSystem &) = delete;

        void render(FrameInfo &frameInfo);
    private:
        void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
        void createPipeline(VkRenderPass renderPass);


        LveDevice& lveDevice;
        std::unique_ptr<LvePipeline> lvePipeline;
        VkPipelineLayout pipelineLayout;
    };

} // lve

#endif //DEV_POINTLIGHTSYSTEM_H
