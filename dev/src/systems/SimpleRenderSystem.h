//
// Created by vox on 12/01/23.
//

#ifndef DEV_POINTLIGHTSYSTEM_H
#define DEV_SIMPLERENDERSYSTEM_H

#include "../LveCamera.h"
#include "../LveDevice.h"
#include "../LvePipeline.h"
#include "../renderables/LveObject.h"
#include "../LveFrameInfo.h"

namespace lve {

    class SimpleRenderSystem {
    public:
        SimpleRenderSystem(LveDevice& device, VkRenderPass renderPass, VkDescriptorSetLayout globalSetLayout);
        ~SimpleRenderSystem();
        SimpleRenderSystem(const SimpleRenderSystem &) = delete;
        SimpleRenderSystem &operator = (const SimpleRenderSystem &) = delete;

        void renderObjects(FrameInfo &frameInfo);
    private:
        void createPipelineLayout(VkDescriptorSetLayout globalSetLayout);
        void createPipeline(VkRenderPass renderPass);


        LveDevice& lveDevice;
        std::unique_ptr<LvePipeline> lvePipeline;
        VkPipelineLayout pipelineLayout;
    };

} // lve

#endif //DEV_POINTLIGHTSYSTEM_H
