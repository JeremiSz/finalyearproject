//
// Created by vox on 06/02/23.
//

#ifndef DEV_LOOPANDBLINNSYSTEM_H
#define DEV_LOOPANDBLINNSYSTEM_H

#include <memory>
#include "../LveCamera.h"
#include "../LveDevice.h"
#include "../LvePipeline.h"
#include "../LveFrameInfo.h"

namespace lve {

    class LoopAndBlinnSystem {
    public:
        LoopAndBlinnSystem(LveDevice& device, VkRenderPass renderPass);
        ~LoopAndBlinnSystem();
        LoopAndBlinnSystem(const LoopAndBlinnSystem &) = delete;
        LoopAndBlinnSystem &operator = (const LoopAndBlinnSystem &) = delete;

        void render(FrameInfo &frameInfo);
    private:
        std::string fragFilepath = "shaders/LoopAndBlinnShader.vert.spv";
        std::string vertFilepath = "shaders/LoopAndBlinnShader.frag.spv";
        std::string geomFilepath = "shaders/LoopAndBlinnShader.geom.spv";
        void createPipelineLayout();
        void createPipeline(VkRenderPass renderPass);
        LveDevice& lveDevice;
        std::unique_ptr<LvePipeline> lvePipeline;
        VkPipelineLayout pipelineLayout;
    };


} // lve

#endif //DEV_LOOPANDBLINNSYSTEM_H
