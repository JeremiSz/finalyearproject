//
// Created by vox on 12/02/23.
//

#ifndef DEV_TESSELLATIONAPP_H
#define DEV_TESSELLATIONAPP_H
#include "../LveWindow.h"
#include "../LveDevice.h"
#include "../LveRenderer.h"
#include "../LveDescriptors.h"
#include "../renderables/LveVectorGraphic.h"

#include <memory>
#include <vector>

namespace lve {

    class TessellationApp {
    public:
        static constexpr int WIDTH = 800, HEIGHT = 800;
        void run();
        void profile(unsigned int i);
        explicit TessellationApp(int deviceIndex);
        ~TessellationApp();
        TessellationApp(const TessellationApp &) = delete;
        TessellationApp &operator = (const TessellationApp &) = delete;
    private:
        void loadObjects();
        LveVectorGraphic loadObject(LveDevice &device, std::string& filepath,glm::vec3 translation,glm::vec3 scale);

        LveWindow lveWindow{WIDTH,HEIGHT,"Hello Vulkan"};
        LveDevice lveDevice;

        LveRenderer lveRenderer{lveWindow,lveDevice};

        std::vector<LveVectorGraphic> graphics;
    };

} // lve

#endif //DEV_TESSELLATIONAPP_H
