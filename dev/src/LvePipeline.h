//
// Created by root on 20/11/22.
//

#ifndef DEV_LVEPIPELINE_H
#define DEV_LVEPIPELINE_H

#include "LveDevice.h"

#include <string>
#include <vector>

namespace lve {
    struct PipelineConfigInfo{
        PipelineConfigInfo() = default;
        PipelineConfigInfo(const PipelineConfigInfo&) = delete;
        PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;

        std::vector<VkVertexInputBindingDescription> bindingDescriptions{};
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};

        VkPipelineViewportStateCreateInfo viewportInfo;
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo;
        VkPipelineRasterizationStateCreateInfo rasterizationInfo;
        VkPipelineMultisampleStateCreateInfo multisampleInfo;
        VkPipelineColorBlendAttachmentState colourBlendAttachment;
        VkPipelineColorBlendStateCreateInfo colourBlendInfo;
        VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
        std::vector<VkDynamicState> dynamicStateEnables;
        VkPipelineDynamicStateCreateInfo dynamicStateInfo;
        VkPipelineLayout pipelineLayout = nullptr;
        VkRenderPass renderPass = nullptr;
        uint32_t subpass = 0;
    };

    class LvePipeline {
    public:
        LvePipeline(LveDevice &device,
                    const std::string& vertFilepath,
                    const std::string& fragFilepath,
                    const std::string* geomFilepath,
                    const std::string* tescFilepath,
                    const std::string* teseFilepath,
                    const PipelineConfigInfo& configInfo);

        ~LvePipeline();
        LvePipeline(const LvePipeline&) = delete;
        LvePipeline &operator = (const LvePipeline&) = delete;
        void bind(VkCommandBuffer commandBuffer);
        static void defaultPipelineConfigInfo(PipelineConfigInfo& configInfo);
        static void addTessellationConfigInfo(PipelineConfigInfo& configInfo);
        static void addQuadSupport(PipelineConfigInfo &configInfo);
    private:
        static std::vector<char> readFile(const std::string& filepath);
        void createGraphicsPipeline(const std::string& vertFilepath,
                                    const std::string& fragFilepath,
                                    const std::string* geomFilepath,
                                    const std::string* tescFilepath,
                                    const std::string* teseFilepath,
                                    const PipelineConfigInfo& configInfo);
        void createShaderModule(const std::vector<char>& code,VkShaderModule* shaderModule);
        LveDevice &lveDevice;
        VkPipeline graphicsPipeline;
        VkShaderModule vertShaderModule;
        VkShaderModule fragShaderModule;
        VkShaderModule geomShaderModule;
        VkShaderModule teseShaderModule;
        VkShaderModule tescShaderModule;
        bool hasGeom{false},hasTess{false};
    };



} // lve

#endif //DEV_LVEPIPELINE_H
