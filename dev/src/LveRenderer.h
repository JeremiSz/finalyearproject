//
// Created by vox on 12/01/23.
//

#ifndef DEV_LVERENDERER_H
#define DEV_LVERENDERER_H

#include <cassert>
#include "LveWindow.h"
#include "LveDevice.h"
#include "LveSwapChain.h"

namespace lve {

    class LveRenderer {
    public:
        LveRenderer(LveWindow &window,LveDevice &device);
        ~LveRenderer();
        LveRenderer(const LveRenderer &) = delete;
        LveRenderer &operator = (const LveRenderer &) = delete;
        VkCommandBuffer beginFrame();
        void endFrame();
        void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
        void endSwapChainRenderPass(VkCommandBuffer commandBuffer);

        VkRenderPass getSwapChainRenderPass() const {return lveSwapChain->getRenderPass();};
        float getAspectRatio() const {return lveSwapChain->extentAspectRatio();};
        bool isFrameInProgress() const {return isFrameStarted;};
        VkCommandBuffer getCurrentCommandBuffer() const;
        int getFrameIndex()const{
            assert(isFrameStarted && "Cannot get frame index when frame not in progress");
            return currentFrameIndex;
        }
    private:
        void createCommandBuffers();
        void freeCommandBuffers();
        void recreateSwapChain();

        LveWindow& lveWindow;
        LveDevice& lveDevice;
        std::unique_ptr<LveSwapChain> lveSwapChain;
        std::vector<VkCommandBuffer> commandBuffers;

        uint32_t currentImageIndex;
        int currentFrameIndex = 0;
        bool isFrameStarted{false};
    };

} // lve

#endif //DEV_LVERENDERER_H
