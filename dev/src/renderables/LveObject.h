//
// Created by vox on 11/01/23.
//

#ifndef DEV_LVEOBJECT_H
#define DEV_LVEOBJECT_H

//#include "LveModel.h"

#include <memory>

#include <glm/gtc/matrix_transform.hpp>
#include <unordered_map>

namespace lve {
    struct TransformComponent{
        glm::vec3 translation{};
        glm::vec3 scale{1.f};
        glm::vec3 rotation{0.0f,0.0f,0.0f};

        glm::mat4 mat4();
        glm::mat3 normalMatrix();
    };

    struct PointLightComponent{
        float lightIntensity = 1.f;

    };

    class LveObject {
    public:
        using id_t = unsigned int;
        using Map = std::unordered_map<id_t,LveObject>;

        static LveObject createObject(){
            static id_t currentId = 0;
            return LveObject{currentId++};
        }

        LveObject(const LveObject &) = delete;
        LveObject &operator=(const LveObject &) = delete;
        LveObject(LveObject &&) = default;
        LveObject &operator=(LveObject &&) = default;

        id_t getId() const{
            return id;
        }
        //std::shared_ptr<LveModel> model{};
        glm::vec3 colour{};
        TransformComponent transform{};
    private:
        LveObject(id_t objId):id(objId){}
        id_t id;
    };
}


#endif //DEV_LVEOBJECT_H
