//
// Created by vox on 06/02/23.
//

#include "LvePath.h"
#include "../LveUtils.h"

#include <cassert>
#include <cstring>

#include <iostream>

namespace lve {

    LvePath::LvePath(LveDevice &device,const LvePath::Builder &builder) : lveDevice(device){
        createVertexBuffers(builder.vertices);
        createIndexBuffers(builder.indices);
    }
    LvePath::~LvePath()= default;

    void LvePath::createVertexBuffers(const std::vector<Vertex> &vertices){
        vertexCount = static_cast<uint32_t>(vertices.size());
        assert(vertexCount >= 3 && "Vertex count must be at least 3");
        VkDeviceSize bufferSize = sizeof(vertices[0]) * vertexCount;
        uint32_t vertexSize = sizeof(vertices[0]);

        LveBuffer stagingBuffer{
                lveDevice,
                vertexSize,
                vertexCount,
                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
        };
        stagingBuffer.map();
        stagingBuffer.writeToBuffer((void *)vertices.data());

        vertexBuffer = std::make_unique<LveBuffer>(
                lveDevice,
                vertexSize,
                vertexCount,
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        );

        lveDevice.copyBuffer(stagingBuffer.getBuffer(),vertexBuffer->getBuffer(),bufferSize);
    }
    void LvePath::createIndexBuffers(const std::vector<uint32_t> &indices) {
        indexCount = static_cast<uint32_t>(indices.size());
        hasIndexBuffer = indexCount > 0;

        if(!hasIndexBuffer){
            return;
        }

        VkDeviceSize bufferSize = sizeof(indices[0]) * indexCount;
        uint32_t indexSize = sizeof(indices[0]);
        LveBuffer stagingBuffer{
                lveDevice,
                indexSize,
                indexCount,
                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
        };
        stagingBuffer.map();
        stagingBuffer.writeToBuffer((void *)indices.data());

        indexBuffer = std::make_unique<LveBuffer>(
                lveDevice,
                indexSize,
                indexCount,
                VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        );

        lveDevice.copyBuffer(stagingBuffer.getBuffer(),indexBuffer->getBuffer(),bufferSize);
    }

    void LvePath::draw(VkCommandBuffer commandBuffer) const{
        if(hasIndexBuffer){
            vkCmdDrawIndexed(commandBuffer,indexCount,1,0,0,0);
        }
        else{
            vkCmdDraw(commandBuffer,vertexCount,1,0,0);
        }

    }
    void LvePath::bind(VkCommandBuffer commandBuffer) {
        VkBuffer buffers[] = {vertexBuffer->getBuffer()};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(commandBuffer,0,1,buffers,offsets);

        if(hasIndexBuffer){
            vkCmdBindIndexBuffer(commandBuffer,indexBuffer->getBuffer(),0,VK_INDEX_TYPE_UINT32);
        }
    }

    std::vector<VkVertexInputBindingDescription> LvePath::Vertex::getBindingDescriptions(){
        std::vector<VkVertexInputBindingDescription> bindingDescriptions(1);
        bindingDescriptions[0].binding = 0;
        bindingDescriptions[0].stride = sizeof(Vertex);
        bindingDescriptions[0].binding = VK_VERTEX_INPUT_RATE_VERTEX;
        return bindingDescriptions;
    }

    std::vector<VkVertexInputAttributeDescription> LvePath::Vertex::getAttributeDescriptions() {
        std::vector<VkVertexInputAttributeDescription> attributeDescriptions{};
        attributeDescriptions.push_back({0,0,VK_FORMAT_R32G32B32_SFLOAT, offsetof(Vertex,position)});
        attributeDescriptions.push_back({1,0,VK_FORMAT_R32G32B32_SFLOAT,offsetof(Vertex,colour)});
        return attributeDescriptions;
    }

    bool LvePath::Vertex::operator==(const LvePath::Vertex &other) const {
        return position == other.position && colour == other.colour;
    }

    std::unique_ptr<LvePath> LvePath::createModelFromFile(LveDevice &device, std::string &filepath){
        Builder builder{};
        builder.loadModel(filepath);
        return std::make_unique<LvePath>(device,builder);
    }
    std::unique_ptr<LvePath> LvePath::createDefault(LveDevice &device) {
        Builder builder{};
        builder.createDefault();
        return std::make_unique<LvePath>(device,builder);
    }

    std::unique_ptr<LvePath> LvePath::createShape(LveDevice &device, std::vector<float> data) {
        Builder builder{};
        builder.createShape(std::move(data));
        return std::make_unique<LvePath>(device,builder);
    }

    void LvePath::Builder::createDefault(){
        vertices.clear();
        indices.clear();
        vertices.push_back({{-1,-1,0},{1,1,1}});
        vertices.push_back({{1,-1,0},{1,1,1}});
        vertices.push_back({{1,1,0},{1,1,1}});
        vertices.push_back({{-1,1,0},{1,1,1}});
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(3);
    }
    void LvePath::Builder::createShape(std::vector<float> data){
        vertices.clear();
        indices.clear();
        for (uint i = 0;i<data.size();i += 2){
            vertices.push_back({{data[i], data[i + 1], 0},{1,1,1}});
            indices.push_back(i/2);
        }
        indices.push_back(0);
    }
    void LvePath::Builder::loadModel(std::string &filepath) {
        vertices.clear();
        indices.clear();
        //tokenize
        std::string delimiter = " ";
        std::vector<std::string> tokens = {};

        /*code from stack overflow
         * Author Vincenzo Pii
         * link https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
         * [Accessed 26/02/2023]
         */
        size_t pos;
        while ((pos = filepath.find(delimiter)) != std::string::npos) {
            std::string token = filepath.substr(0, pos);
            tokens.push_back(token);
            filepath = filepath.substr(pos + delimiter.length());
        }
        //end of code sample
        tokens.push_back(filepath);
        std::cout << tokens.size() << std::endl;

        auto deli = ",";
        size_t index = 0;
        glm::vec2 previous_point = {0,0};
        bool isUpper = false;
        while (!tokens.empty()){
            std::string token = tokens.at(0);
            if (token == "m" || token == "M"){
                tokens.erase(tokens.begin());
                isUpper = isupper(token[0]);
                auto coord = tokens.at(0);
                tokens.erase(tokens.begin());

                auto del_index = coord.find(deli);
                 auto x = std::stof(coord.substr(0, del_index));
                 auto y = std::stof(coord.substr(del_index + 1));

                if (isUpper){
                    previous_point = {x,y};
                }
                else{
                    previous_point += glm::vec2{x,y};
                }

                Vertex vertex{};
                vertex.position = {previous_point.x,previous_point.y,0};
                vertex.colour = {1.f,1.f,1.f};
                vertices.push_back(vertex);
            }
            else if(token == "c" || token == "C"){
                tokens.erase(tokens.begin());
                isUpper = isupper(token[0]);
                indices.push_back(index++);
                for(size_t i = 0;i<3;i++){
                    auto coord = tokens.at(0);
                    tokens.erase(tokens.begin());
                    auto del_index = coord.find(deli);
                    auto x = std::stof(coord.substr(0, del_index));
                    auto y = std::stof(coord.substr(del_index + 1));

                    if (isUpper){
                        previous_point = {x,y};
                    }
                    else{
                        previous_point += glm::vec2{x,y};
                    }
                    Vertex vertex{};
                    vertex.position = {previous_point.x,previous_point.y,0};
                    vertex.colour = {1.f,1.f,1.f};
                    vertices.push_back(vertex);
                    indices.push_back(index++);
                }

            }
            else if(token == "z" || token == "Z"){
                tokens.erase(tokens.begin());
                indices.push_back(index);
                indices.push_back(index);
                indices.push_back(0);
                indices.push_back(0);
                break;
            }
            else{
                indices.push_back(index++);
                for(size_t i = 0;i<3;i++){
                    auto coord = tokens.at(0);
                    tokens.erase(tokens.begin());
                    auto del_index = coord.find(deli);
                    auto x = std::stof(coord.substr(0, del_index));
                    auto y = std::stof(coord.substr(del_index + 1));

                    if (isUpper){
                        previous_point = {x,y};
                    }
                    else{
                        previous_point += glm::vec2{x,y};
                    }
                    Vertex vertex{};
                    vertex.position = {previous_point.x,previous_point.y,0};
                    vertex.colour = {1.f,1.f,1.f};
                    vertices.push_back(vertex);
                    indices.push_back(index++);
                }
            }
        }

        std::cout << "finished"<<std::endl;
        std::cout << vertices.size() << " " << indices.size() << std::endl;
        for (auto vert : vertices){
            std::cout << vert.position.x << " " << vert.position.y <<" test";
        }
        std::cout << std::endl;
        for(auto index : indices){
            std::cout << index << " test";
        }
        std::cout << std::endl;
    }

} // lve