//
// Created by vox on 06/02/23.
//

#ifndef DEV_LVEPATH_H
#define DEV_LVEPATH_H
#include "../LveDevice.h"
#include "../LveBuffer.h"
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <vector>
#include <memory>

namespace lve {

    class LvePath {
    public:
        struct Vertex{
            glm::vec3 position{};
            glm::vec3 colour{};

            static std::vector<VkVertexInputBindingDescription> getBindingDescriptions();
            static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();
            bool operator==(const Vertex &other) const;
        };
        struct Builder{
            std::vector<Vertex> vertices{};
            std::vector<uint32_t> indices{};

            void loadModel(std::string &filepath);
            void createDefault();

            void createShape(std::vector<float> data);
        };
        LvePath(LveDevice &device,const LvePath::Builder &builder);
        ~LvePath();
        LvePath(const LvePath &) = delete;
        LvePath &operator=(const LvePath &) = delete;

        static std::unique_ptr<LvePath> createModelFromFile(LveDevice &device, std::string &filepath);

        void bind(VkCommandBuffer commandBuffer);
        void draw(VkCommandBuffer commandBuffer) const;
        void createVertexBuffers(const std::vector<Vertex> &vertices);
        void createIndexBuffers(const std::vector<uint32_t> &indices);
        static std::unique_ptr<LvePath> createDefault(LveDevice &device);
        static std::unique_ptr<LvePath> createShape(LveDevice &device, std::vector<float> data);
    private:
        LveDevice& lveDevice;
        std::unique_ptr<LveBuffer> vertexBuffer;
        uint32_t vertexCount;

        bool hasIndexBuffer = false;
        std::unique_ptr<LveBuffer> indexBuffer;
        uint32_t indexCount;
    };

} // lve

#endif //DEV_LVEPATH_H
