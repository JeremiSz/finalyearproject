//
// Created by vox on 06/02/23.
//

#ifndef DEV_LVEVECTORGRAPHIC_H
#define DEV_LVEVECTORGRAPHIC_H

#include <memory>

#include <glm/gtc/matrix_transform.hpp>
#include <unordered_map>
#include "LvePath.h"

namespace lve {
    struct TransformVectorComponent{
        glm::vec3 translation{};
        glm::vec3 scale{1.f};
        glm::vec3 rotation{0.0f,0.0f,0.0f};

        glm::mat4 mat4();
        glm::mat3 normalMatrix();
    };

    class LveVectorGraphic {
    public:
        LveVectorGraphic()= default;
        LveVectorGraphic(const LveVectorGraphic &) = delete;
        LveVectorGraphic &operator=(const LveVectorGraphic &) = delete;
        LveVectorGraphic(LveVectorGraphic &&) = default;
        LveVectorGraphic &operator=(LveVectorGraphic &&) = default;

        std::shared_ptr<LvePath> model{};
        glm::vec3 colour{};
        TransformVectorComponent transform{};
    };

} // lve

#endif //DEV_LVEVECTORGRAPHIC_H
