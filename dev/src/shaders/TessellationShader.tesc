#version 450

layout(vertices = 4) out;

layout(location = 0) in vec3 colour[];
layout(location = 0) out vec3 fragColour[];

void main(void) {
    //gl_TessLevelInner[0] = 0.0;
    gl_TessLevelOuter[0] = 1.0;
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    fragColour[gl_InvocationID] = colour[gl_InvocationID];
}