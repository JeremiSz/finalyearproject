#version 450

layout(isolines,equal_spacing,ccw) in;
layout(location = 0) in vec3 colour[];
layout(location = 0) out vec3 fragColour;

vec4 interpolate(vec4 p1, vec4 p2, vec4 p3,vec4 p4){
    vec4 a = mix(p1,p2,gl_TessCoord.x);
    vec4 b = mix(p3,p4,gl_TessCoord.x);
    return mix(a,b,gl_TessCoord.y);
}


void main(void) {
    gl_Position = interpolate(gl_in[0].gl_Position,
                                gl_in[1].gl_Position,
                                gl_in[2].gl_Position,
                                gl_in[3].gl_Position
                                );
    fragColour = colour[gl_PrimitiveID];
}