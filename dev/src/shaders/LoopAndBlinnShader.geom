#version 450

layout(lines_adjacency) in;
layout(triangle_strip,max_vertices = 6) out;

layout(location = 0) in vec3 colour[];
layout (location = 1) out vec2 fragUV;
layout(location = 0) out vec3 fragColour;

void main() {
    //import points
    vec3 b0 = gl_in[0].gl_Position.xyz;
    vec3 b1 = gl_in[1].gl_Position.xyz;
    vec3 b2 = gl_in[2].gl_Position.xyz;
    vec3 b3 = gl_in[3].gl_Position.xyz;

    //from cubic bezuir to catmull-rom
    //code adapted from https://github.com/ttnghia/QuadraticApproximation/blob/master/Source/QuadraticCurveApproximation.cpp
    //Author: Soroosh Tayebi Arasteh
    //Accessed 12/03/2023
    float gamma = .5;
    vec3 q0 = b0;
    vec3 q5 = b3;
    vec3 q1 = b0 + 1.5 * gamma * (b1 - b0);
    vec3 q4 = b3 - 1.5 * (1.0 - gamma) * (b3 - b2);
    vec3 q2and3 = mix(q1,q4,gamma);
    //end of code segment
    gl_Position = vec4(q0,1);
    fragUV = vec2(1.0,1.0);
    fragColour = colour[0];
    EmitVertex();

    gl_Position = vec4(q1,1);
    fragUV = vec2(0.5,0.0);
    fragColour = colour[1];
    EmitVertex();

    gl_Position = vec4(q2and3,1);
    fragUV = vec2(0.0,0.0);
    fragColour = colour[2];
    EmitVertex();
    gl_Position = vec4(q2and3,1);
    fragUV = vec2(1.0,1.0);
    fragColour = colour[1];
    EmitVertex();

    gl_Position = vec4(q4,1);
    fragUV = vec2(0.5,0.0);
    fragColour = colour[2];
    EmitVertex();

    gl_Position = vec4(q5,1);
    fragUV = vec2(0.0,0.0);
    fragColour = colour[3];
    EmitVertex();
    EndPrimitive();
}