#version 450

layout(location = 0) in vec3 fragColour;
layout(location = 1) in vec2 fragUV;

layout(location = 0) out vec4 outColour;

void main(){
    float alpha = (fragUV.x * fragUV.x) - fragUV.y;
    if(!(alpha<0) ){
        discard;
    }
    outColour = vec4(fragColour,1.0);
}