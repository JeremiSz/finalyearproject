//
// Created by root on 20/11/22.
//
#include <stdexcept>
#include "LveWindow.h"

namespace lve{
    LveWindow::LveWindow(int width, int height, std::string name) : width{width},height{height},windowName{name}{
        initWindow();
    }

    LveWindow::~LveWindow() {
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    void LveWindow::initWindow() {
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API,GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE,GLFW_TRUE);

        window = glfwCreateWindow(width,height,windowName.c_str(), nullptr, nullptr);
        glfwSetWindowUserPointer(window, this);
        glfwSetFramebufferSizeCallback(window,frameBufferResizeCallback);
    }
    bool LveWindow::shouldClose() {
        return glfwWindowShouldClose(window);;
    }
    void LveWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface){
        if (glfwCreateWindowSurface(instance,window, nullptr,surface) != VK_SUCCESS){
            throw std::runtime_error("Failed to create window surface");
        }
    }
    VkExtent2D LveWindow::getExtent() {
        return {static_cast<uint32_t>(width),static_cast<uint32_t>(height)};
    }
    void LveWindow::frameBufferResizeCallback(GLFWwindow *window,int width,int height){
        auto lveWindow = reinterpret_cast<LveWindow *>(glfwGetWindowUserPointer(window));
        lveWindow->frameBufferResized = true;
        lveWindow->width = width;
        lveWindow->height = height;
    }
}
