cd ../build/shaders || return
rm ./*
/usr/bin/glslc -c ../../src/shaders/*.vert
/usr/bin/glslc -c ../../src/shaders/*.frag
/usr/bin/glslc -c ../../src/shaders/*.geom
/usr/bin/glslc -c ../../src/shaders/*.tese
/usr/bin/glslc -c ../../src/shaders/*.tesc