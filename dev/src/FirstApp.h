//
// Created by root on 20/11/22.
//

#ifndef DEV_FIRSTAPP_H
#define DEV_FIRSTAPP_H

#include "LveWindow.h"
#include "LveDevice.h"
#include "LveRenderer.h"
#include "LveDescriptors.h"
#include "renderables/LveVectorGraphic.h"

#include <memory>
#include <vector>

namespace lve {

    class FirstApp {
    public:
        static constexpr int WIDTH = 800, HEIGHT = 800;
        void run();
        FirstApp():lveDevice(lveWindow, true, true,0){};
        ~FirstApp();
        FirstApp(const FirstApp &) = delete;
        FirstApp &operator = (const FirstApp &) = delete;
    private:
        void loadObjects();
        LveVectorGraphic loadObject(LveDevice &device, const std::string& filepath,glm::vec3 translation,glm::vec3 scale);

        LveWindow lveWindow{WIDTH,HEIGHT,"Hello Vulkan"};

        LveDevice lveDevice;

        LveRenderer lveRenderer{lveWindow,lveDevice};

        std::vector<LveVectorGraphic> graphics;
    };

} // lve

#endif //DEV_FIRSTAPP_H
