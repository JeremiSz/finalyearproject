#include "FirstApp.h"
#include "systems/LoopAndBlinnApp.h"
#include "systems/TessellationApp.h"
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <cstring>
enum Systems {
    LoopAndBlinn,
    Tessellation
};


int main(int argc, char *argv[]) {
    assert(argc >= 3 && "Must spesify loop and Blinn (-l) or Tessellation (-t) and device");
    int currentSystem;
    if (strcmp(argv[1], "-l") == 0) {
        currentSystem = 0;
    }
    else if (strcmp(argv[1], "-t") == 0)
        currentSystem = 1;
    else {
        std::cerr << "only -l and -t are supported" << std::endl;
        return EXIT_FAILURE;
    }
    int device = std::stoi(argv[2]);
    try{
        if(argc < 4){

            switch (currentSystem) {
                case LoopAndBlinn:
                    lve::LoopAndBlinnApp{device}.run();
                    break;
                case Tessellation:
                    lve::TessellationApp{device}.run();
                    break;
            }
        }
        else{
            int frames = std::stoi(argv[3]);
            switch (currentSystem) {
                case LoopAndBlinn:
                    lve::LoopAndBlinnApp{device}.profile(frames);
                    break;
                case Tessellation:
                    lve::TessellationApp{device}.profile(frames);
                    break;
            }
        }
    }
    catch (const std::exception &e){
        std::cerr << e.what() << "\n";
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
