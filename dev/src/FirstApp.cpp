//
// Created by root on 20/11/22.
//

#include "FirstApp.h"
#include "KeyboardMovementController.h"
#include "LveFrameInfo.h"
#ifdef USE_LOOP_AND_BLINN
#include "systems/LoopAndBlinnSystem.h"
#endif
#ifdef USE_TESSELATION
#include "systems/TessellationSystem.h"
#endif

#include <array>
#include <chrono>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>



namespace lve {
    FirstApp::FirstApp() {
        loadObjects();
    }

    FirstApp::~FirstApp() {}

    void FirstApp::run() {
#ifdef USE_LOOP_AND_BLINN
        LoopAndBlinnSystem loopAndBlinnSystem{lveDevice, lveRenderer.getSwapChainRenderPass()};
#endif
#ifdef USE_TESSELATION
        TessellationSystem tesselationSystem{lveDevice, lveRenderer.getSwapChainRenderPass()};
#endif
        auto startTime = std::chrono::high_resolution_clock::now();
        while (!lveWindow.shouldClose()){
            glfwPollEvents();
            auto newTime = std::chrono::high_resolution_clock::now();
            float frameTime = std::chrono::duration<float,std::chrono::seconds::period>(newTime - startTime).count();
            startTime = newTime;

            if (auto commandBuffer = lveRenderer.beginFrame()){
                int frameIndex = lveRenderer.getFrameIndex();
                FrameInfo frameInfo{
                    frameIndex,
                    frameTime,
                    commandBuffer,
                    graphics
                };
                //render
                lveRenderer.beginSwapChainRenderPass(commandBuffer);
#ifdef USE_LOOP_AND_BLINN
                loopAndBlinnSystem.render(frameInfo);
#endif
#ifdef USE_TESSELATION
                tesselationSystem.render(frameInfo);
#endif
                lveRenderer.endSwapChainRenderPass(commandBuffer);
                lveRenderer.endFrame();
            }
        }
        vkDeviceWaitIdle(lveDevice.device());
    }

    void FirstApp::loadObjects() {
        auto vase = loadObject(
                lveDevice,
                "data/models/cube.obj",
                {0.f, 0.f, 1.f},
                {.5f, .5f, .5f}
                );
        graphics.push_back(std::move(vase));
    }
    LveVectorGraphic FirstApp::loadObject(LveDevice& device, const std::string& filepath,glm::vec3 translation,glm::vec3 scale){
        auto object = LveVectorGraphic();
        object.model = LvePath::createModelFromFile(device,filepath);
        object.transform.translation = translation;
        object.transform.scale = scale;
        return object;
    }
} // lve